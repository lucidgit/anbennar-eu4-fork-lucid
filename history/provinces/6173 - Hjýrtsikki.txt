# í <-- This is here to make sure that the encoding stays ANSI, do not remove it
culture = forest_troll
religion = mountain_watchers
capital = "Hjýrtsikki"
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 2
base_manpower = 2
native_size = 70
native_ferocity = 8
native_hostileness = 8