# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
culture = kukatodic
religion = kukatodic_religion
hre = no

base_tax = 1
base_production = 1
base_manpower = 1
native_size = 2
native_ferocity = 3
native_hostileness = 4

trade_goods = unknown

capital = ""