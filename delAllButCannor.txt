# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
every_province = {
	limit = { NOT = { continent = europe } }
	cede_province = ---
	set_province_flag = cant_colonize
	destroy_province = yes
}