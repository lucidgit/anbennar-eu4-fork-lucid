# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
#Endralliande
1004 = "Aldanc�st"
1009 = "Alarian Reigh"
2230 = "Ard Alarianvar"
2828 = "�sel Alarianvar"

#Ruined Isles
1052 = "Calinaire"
1086 = "Calas�il"
1101 = "�ileanvi�"
1104 = "C�sith�il"

#Bloodgroves
1016 = "Cannain�c�st"
1110 = "Fadhcannain�"

#Trollsbay
1019 = "Camnarionn"
1021 = "Tuguilael"
1023 = "N�r Sl�ibhe"
1024 = "Varillin"
1026 = "Aistiande"
1027 = "Seluslitr�"
1028 = "Ain�abeahn"
1029 = "Istralaghin"
1030 = "Erelael"
1035 = "Lithc�st"
1036 = "Lith�il"
1037 = "Petagth�l"
1043 = "Laochsl�ibhe"
1044 = "Laochaire"
1046 = "Lanelmas"
1047 = "Arethc�st"
1049 = "Alar�nne"
1126 = "Andein"
1871 = "Cestirith"
1873 = "Ain�tenach"
1952 = "Eaioned�illa"
2563 = "Serenionn"

#Reaper's Coast
1939 = "Seinfeur"
1948 = "C�sitenach"

#Soruin
2175 = "Alarianin"
2180 = "Alarian Feurlar"
2199 = "Sorn Rialaire"

#Broken Isles
2516 = "Arbreigh"

#Dry Coast
2296 = "Oscurian"
2302 = "Serencas"
2305 = "Serenvi�"

#Severed Coast
2328 = "P�r Obaith�il"

#Amadia
2403 = "Munasc�st"
2417 = "Lithiellen"
2422 = "Dahvarleth"
2424 = "S�crheivar"
2446 = "Aistvar"