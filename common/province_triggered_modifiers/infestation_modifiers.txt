# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
infestation_monthly_pulse_global = {
	potential = {
		always = yes
	}

	trigger = {
		OR = {
			AND = {
				is_month = 0
				NOT = { is_month = 1 }
			}
			AND = {
				is_month = 2
				NOT = { is_month = 3 }
			}
			AND = {
				is_month = 4
				NOT = { is_month = 5 }
			}
			AND = {
				is_month = 6
				NOT = { is_month = 7 }
			}
			AND = {
				is_month = 8
				NOT = { is_month = 9 }
			}
			AND = {
				is_month = 10
				NOT = { is_month = 11 }
			}
		}
	}
	
	on_activation = {
		province_event = { id = infestation.1 }
	}

	hidden = yes
	
	on_deactivation = {}
}

#for innfestations

infestation_harpy_monthly_pulse = {
	potential = {
		always = yes
	}

	trigger = {
		had_province_flag = { # we don't want to immediatly start firing events
			flag = infestation_pulse_flag
			days = 100
		}
		NOT = { is_month = 1 } #fires on the first month of the year, once per year
	}
	
	on_activation = {
		province_event = { id = infestation_harpy.1 days = 0 random = 300 }
	}

	hidden = yes
	
	on_deactivation = {}
}

infestation_goblin_monthly_pulse = {
	potential = {
		always = yes
	}

	trigger = {
		had_province_flag = { # we don't want to immediatly start firing events
			flag = infestation_pulse_flag
			days = 100
		}
		NOT = { is_month = 1 } #fires on the first month of the year, once per year
	}
	
	on_activation = {
		province_event = { id = infestation_goblin.1 days = 0 random = 300 }
	}

	hidden = yes
	
	on_deactivation = {}
}

infestation_zombie_monthly_pulse = {
	potential = {
		always = yes
	}

	trigger = {
		had_province_flag = { # we don't want to immediatly start firing events
			flag = infestation_pulse_flag
			days = 100
		}
		NOT = { is_month = 1 } #fires on the first month of the year, once per year
	}
	
	on_activation = {
		province_event = { id = infestation_zombie.1 days = 0 random = 300 }
	}

	hidden = yes
	
	on_deactivation = {}
}

infestation_merathis_monthly_pulse = {
	potential = {
		always = yes
	}

	trigger = {
		had_province_flag = { # we don't want to immediatly start firing events
			flag = infestation_pulse_flag
			days = 100
		}
		NOT = { is_month = 1 } #fires on the first month of the year, once per year
	}
	
	on_activation = {
		province_event = { id = infestation_dragon_merathis.1 days = 0 random = 300 }
	}

	hidden = yes
	
	on_deactivation = {}
}

#recursion_example_modifier = {
#	hidden = yes #this works
#
#	potential = {
#		always = yes
#	}
#
#	trigger = {
#		had_province_flag = {
#			flag = foo
#			days = bar
#		}
#	}
#	
#	on_activation = {
#		province_event = { id = myevent.0 } #does stuff and removes the flag
#	}
#
#	
#	on_deactivation = {
#		province_event = { id = myevent.1 } #adds the flag again
#	}
#}