# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Jhorgashirr Square

unit_type = tech_eordand
type = infantry

maneuver = 1
offensive_morale = 1
defensive_morale = 2
offensive_fire = 2
defensive_fire = 1
offensive_shock = 2
defensive_shock = 2