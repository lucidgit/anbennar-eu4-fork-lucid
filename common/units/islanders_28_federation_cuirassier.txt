# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Western Medieval Knights

type = cavalry
unit_type = tech_islanders

maneuver = 2
offensive_morale = 5
defensive_morale = 4
offensive_fire = 2
defensive_fire = 2
offensive_shock = 5
defensive_shock = 3
