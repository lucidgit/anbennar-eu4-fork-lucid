# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Swedish Gallop

type = cavalry
unit_type = tech_elven

maneuver = 2
offensive_morale = 4
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 5
defensive_shock = 3
