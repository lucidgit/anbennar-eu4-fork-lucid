# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
bookmark =
{
	name = "BULWAR_NAME"
	desc = "BULWAR_DESC"
	date = 1444.11.11
	
	center = 601
	default = no
	
	country = F37	#Irrliam
	country = F42	#Varamhar
	country = F25	#Sareyand
	
	country = F22	#Dartax
	country = F24	#Brasan
	
	country = U01	#Kheterata
	country = U05	#Elizna
	
	country = F29	#Zokka
	country = U07	#Viakkoc
	
	country = U15	#Aqatbar
	
	country = F27	#Harpylen
	country = F46	#Jaddari

	easy_country = F37
	easy_country = F25
	easy_country = U01
	easy_country = F29

}